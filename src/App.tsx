import React from 'react';
import CanvasJSReact from './canvasjs.react';
import { parseColumn, getOptions, getDataPoints, TopColorItem } from './dataPoint';
import TwoThumbsDraggableTrack from './range';
import Select from 'react-select';
import * as options from './options';
import './App.css';
import { MaterialDesignSwitch } from './switch';
import { createTopColorList, defaultTopColors } from './colorData';
import { RGBColor } from 'react-color'
import { ColorPicker } from './colorPicker';

let CanvasJS = CanvasJSReact.CanvasJS;
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

interface TimeInterval {
	from: number,
	to: number,
}

// let handleChange = (selectedOption: string, setFunction: React.Dispatch<React.SetStateAction<string>>) => {
// 	setFunction(selectedOption);
// }

function App() {
	const [error, setError] = React.useState<any>(null);
	const [isLoaded, setIsLoaded] = React.useState(false);

	const [timeInterval, setTimeInterval] = React.useState<TimeInterval>({from: 2000, to: 2005})

	const [label, setLabel] = React.useState<string>('title_english');
	const [labelData, setLabelData] = React.useState<string[]>([]);

	const [xAxis, setXAxis] = React.useState<string>('score');
	const [xAxisData, setXAxisData] = React.useState<any[]>([]);

	const [yAxis, setYAxis] = React.useState<string>('rank');
	const [yAxisData, setYAxisData] = React.useState<any[]>([]);

	const [sizeData, setSizeData] = React.useState<any[]>([]);

	const [color, setColor] = React.useState<string>('genre');
	const [colorData, setColorData] = React.useState<any[]>([]);
	const [topColorList, setTopColorList] = React.useState<TopColorItem[]>([]);
	const [topColors, setTopColors] = React.useState<RGBColor[]>(defaultTopColors);

	const [head, setHead] = React.useState<number>(10);
	const [toggleLabels, setToggleLabels] = React.useState<boolean>(false);
  
	// Примечание: пустой массив зависимостей [] означает, что
	// этот useEffect будет запущен один раз
	// аналогично componentDidMount()

	React.useEffect(() => {
		// console.log("Time Interval: ", timeInterval)
		fetch("https://anime.yazasnyal.dev/anime-visual/label/" + label + "/xaxis/"  + xAxis + "/yaxis/" + yAxis + "/color/" + color + "/from/" + timeInterval.from + "/to/" + timeInterval.to + "/head/" + head)
			.then(res => res.json())
			.then(
			(result) => {
				// console.log("Result: ", result);
				setIsLoaded(true);
				setLabelData(parseColumn(result["label"]));
				setXAxisData(parseColumn(result["xaxis"]));
				setYAxisData(parseColumn(result["yaxis"]));
				setColorData(parseColumn(result["color"]));
				setSizeData(parseColumn(result["size"]));
				setTopColorList(createTopColorList(parseColumn(result["topColors"]), topColors));
			},
			// Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
			// чтобы не перехватывать исключения из ошибок в самих компонентах.
			(error) => {
				setIsLoaded(true);
				setError(error);
			}
		)
	}, [timeInterval, label, xAxis, yAxis, color, head, topColors])

	// console.log("label: ", labelData);
	// console.log("XAxis: ", xAxisData);
	// console.log("YAxis: ", yAxisData);
	console.log("Top Colors: ", topColorList);
  
	
	if (error) {
	  return <div>Ошибка: {error.message}</div>;
	} else if (!isLoaded) {
	  return <div>Загрузка...</div>;
	} else {
	  return (
		<div className="main">
			{/* {console.log("Color Data: ", colorData)} */}
			{/* {console.log("Color TOP 5: ", getTopColors(colorData, 5))} */}
			<div className="chart">
				<CanvasJSChart options = {getOptions(getDataPoints(labelData, xAxisData, yAxisData, colorData, topColorList, sizeData), toggleLabels, xAxis, yAxis)} key={'main' + timeInterval.from + timeInterval.to}
					/* onRef={ref => this.chart = ref} */
				/>
			</div>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
			<div className="years">
				<TwoThumbsDraggableTrack rtl={false} setInterval={setTimeInterval}></TwoThumbsDraggableTrack>
			</div>
			<div className="options">
				<h1>Options Selector</h1>
				<h2>Color</h2>
				<Select options={options.color} defaultValue={{value: 'genre', label: 'Genre'}} onChange={(e) => setColor(e!.value)}></Select>
				<h2>Top 5 most popular</h2>
				<ul>
					{topColorList.map((value, index) => {
						return (<li key={index}>
							<ColorPicker topColorList={topColorList} setTopColorList={setTopColorList} index={index} defaultColor={value.color}></ColorPicker>
							<div className="color-text">
								{value.colorHolder}
							</div>
						</li>)
					})}
				</ul>
				
				<MaterialDesignSwitch setToggleLabels={setToggleLabels}></MaterialDesignSwitch>
				{toggleLabels && 
				<div>
					<h2>Label</h2>
					<Select options={options.label} defaultValue={{value: 'title_english', label: 'Title English'}} onChange={(e) => setLabel(e!.value)}></Select>
				</div>
				}
				<h2>Number of titles (selected at random) to display</h2>
				<Select options={options.head} defaultValue={{value: '10', label: '10'}} onChange={(e) => setHead(Number(e!.value))}></Select>
				<h2>X Axis</h2>
				<Select options={options.axis} defaultValue={{value: 'score', label: 'Score'}} onChange={(e) => setXAxis(e!.value)}></Select>
				<h2>Y Axis</h2>
				<Select options={options.axis} defaultValue={{value: 'rank', label: 'Rank'}} onChange={(e) => setYAxis(e!.value)}></Select>
			</div>
		</div>
	  );
	}
  }

export default App;
