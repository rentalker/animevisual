import React from 'react';

export interface Anime {
    id: number;
    name: string;
    genre: string[];
    source: string;
    studio: string[];
    score: number;
    scored_by: number;
    rank: number;
    popularity: number;
    favorites: number;
}

export interface AnimeJSON {
    animeID: string[]
    title_english: string[]
}

export function draw(animeList: Anime[]) {
    
}