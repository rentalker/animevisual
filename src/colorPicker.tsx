import React from 'react'
import reactCSS from 'reactcss'
import { SketchPicker, RGBColor } from 'react-color'
import { TopColorItem } from './dataPoint';

// interface RGB {
//     r: string,
//     g: string,
//     b: string,
//     a: string,
// }

export const ColorPicker: React.FC<{ topColorList: TopColorItem[], setTopColorList: any, defaultColor: RGBColor, index: number}> = ({topColorList, setTopColorList, defaultColor, index}) => {
    const [displayColorPicker, setDisplayColorPicker] = React.useState<boolean>(false);
    const [color, setColor] = React.useState<RGBColor>(defaultColor);

    const styles = reactCSS({
      'default': {
        color: {
          width: '36px',
          height: '14px',
          borderRadius: '2px',
          background: `rgba(${ color.r }, ${ color.g }, ${ color.b }, ${ color.a })`,
        },
        swatch: {
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: 2,
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    return (
      <div>
        <div style={ styles.swatch } onClick={() => setDisplayColorPicker(!displayColorPicker) }>
          <div style={ styles.color } />
        </div>
        { displayColorPicker && 
            <div style={ { position: 'absolute', zIndex: 2} }>
                <div style={{ 
                    position: 'fixed',
                    top: '0px',
                    right: '0px',
                    bottom: '0px',
                    left: '0px',
                    }} onClick={() => setDisplayColorPicker(false)}>
                </div>
                <SketchPicker color={ color } onChange={(e) => {
                    setColor(e.rgb);
                    setTopColorList(changeTopColors(topColorList, index, e.rgb));
                    // changeTopColors(topColorList, index, e.rgb)
                    }} />
            </div>
        }
      </div>
    )
}

function changeTopColors(topColorList: TopColorItem[], index: number, color: RGBColor): TopColorItem[] {
  let topColorListCloned = [...topColorList];
  topColorListCloned[index].color = color;
  return topColorListCloned;
}
