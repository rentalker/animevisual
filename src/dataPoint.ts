// import * as d3 from 'd3';
import { RGBColor } from 'react-color'

export interface DataPoint {
    label: string,
    x: number,
    y: number,
    z?: number,
    color?: string,
}

export interface TopColorItem {
    color: RGBColor,
    colorHolder: string,
}

export function ArrayCommonElement(colorItems: string[], topColorList: TopColorItem[]): TopColorItem | null {
    for (let topColorsItem of topColorList) {
        if (colorItems.includes(topColorsItem.colorHolder)) {
            return topColorsItem;
        }
    }
    return null;
}

export function parseColumn(items: any[]): string[] {
    // return [];
    let dataArray: string[] = [];
    for (let key in items) {
        dataArray.push(items[key]);
    }
    return dataArray;
}

function setItemColor(colorItems: string[], topColorList: TopColorItem[]): string {
    if (!colorItems) {
        return 'grey';
    }

    let topColorItem = ArrayCommonElement(colorItems, topColorList);
    if (topColorItem) {
        return `rgba(${ topColorItem.color.r }, ${ topColorItem.color.g }, ${ topColorItem.color.b }, ${ topColorItem.color.a })`;
    } else {
        return 'grey';
    }
}

export function getDataPoints(labelData: any[], xAxisData: any[], yAxisData: any[], colorData: any[], topColors: TopColorItem[], sizes: any[] ): DataPoint[] {
    // let color = undefined;
    // if (colorData) {

    let dataPoints: DataPoint[] = [];
    for (let i = 0; i < labelData.length; i++) {
        // if (colorData )
        let dp: DataPoint = {
            label: labelData[i],
            x: xAxisData[i],
            y: yAxisData[i],
            z: sizes[i],
            color: setItemColor(colorData[i], topColors),
        }
        dataPoints.push(dp);
    }

    return dataPoints;
}

export function getOptions(dataPoints: DataPoint[], toggleLabels: boolean, xAxis: string, yAxis: string) {
    return {
        animationEnabled: true,
        exportEnabled: true,
        theme: "dark2", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Anime graphics",
        fontSize: 26
        },
        axisX: {
            title: xAxis,
        // logarithmic: true
        },
        axisY: {
            title: yAxis
        },
        data: [{
            type: "bubble",
            // markerSize: 10,
            markerBorderColor : "black",
            markerBorderThickness: 2,
            indexLabel: toggleLabels ? "{label}" : "",
            toolTipContent: "<b>{label}</b><br>" + xAxis + ": {x}<br>" + yAxis + ": {y}<br>Members: {z}",
            dataPoints: dataPoints
        }]
    };
}