import React from 'react';
import { Range } from 'react-range';
import { getTrackBackground } from './range_types';

const STEP = 1;
const MIN = 1998;
const MAX = 2019;

// Copy of TwoThumbs with `draggableTrack` prop added
const TwoThumbsDraggableTrack: React.FC<{ rtl: boolean, setInterval: any }> = ({ rtl, setInterval }) => {
  const [values, setValues] = React.useState([2000, 2005]);
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap'
      }}
    >
      <Range
        draggableTrack={false}
        values={values}
        step={STEP}
        min={MIN}
        max={MAX}
        rtl={rtl}
        onChange={(values) => {
          setValues(values);
        }}
        onFinalChange={(values) => {
            setInterval({from: values[0], to: values[1]})
        }}
        renderTrack={({ props, children }) => (
          <div
            onMouseDown={props.onMouseDown}
            onTouchStart={props.onTouchStart}
            style={{
              ...props.style,
              height: '36px',
              display: 'flex',
              width: '100%'
            }}
          >
            <div
              ref={props.ref}
              style={{
                height: '5px',
                width: '100%',
                borderRadius: '4px',
                background: getTrackBackground({
                  values,
                  colors: ['#ccc', '#548BF4', '#ccc'],
                  min: MIN,
                  max: MAX,
                  rtl
                }),
                alignSelf: 'center'
              }}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ props, isDragged }) => (
          <div
            {...props}
            style={{
              ...props.style,
              height: '42px',
              width: '42px',
              borderRadius: '4px',
              backgroundColor: '#FFF',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              boxShadow: '0px 2px 6px #AAA'
            }}
          >
            <div
              style={{
                height: '16px',
                width: '5px',
                backgroundColor: isDragged ? '#548BF4' : '#CCC'
              }}
            />
          </div>
        )}
      />
      <output style={{ marginTop: '30px' }} id="output">
        <h3>
          Years: {values[0]} - {values[1]}
        </h3>  
      </output>
    </div>
  );
};

export default TwoThumbsDraggableTrack;