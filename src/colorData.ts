import { RGBColor } from "react-color";
import { TopColorItem } from "./dataPoint";


export function createTopColorList(topColorHolders: string[], topColors: RGBColor[]): TopColorItem[] {
    let topColorList: TopColorItem[] = [];
    for (let i = 0; i < topColorHolders.length; i++) {
        let topColorItem: TopColorItem = {
            color: topColors[i],
            colorHolder: topColorHolders[i],
        }
        topColorList.push(topColorItem);
    }
    return topColorList;
}

export const defaultTopColors: RGBColor[] = [
    {
        r: 101,
        g: 90,
        b: 124,
        a: 1,
    },
    {
        r: 171,
        g: 146,
        b: 191,
        a: 1,
    },
    {
        r: 175,
        g: 193,
        b: 214,
        a: 1,
    },
    {
        r: 246,
        g: 249,
        b: 242,
        a: 1,
    },
    {
        r: 214,
        g: 202,
        b: 152,
        a: 1,
    },
];

// function getTopColors(colorData: any, topn: number): string[] {
//     let ans: string[] = [];
//     let sizes = getColorSizes(colorData);
//     sizes[Symbol.iterator] = function* () {
//     yield* [...this.entries()].sort((a, b) => a[1] - b[1]);
//     }

//     for (let i = 0; i<topn; i++) {
//         ans.push(sizes.keys().next().value);
//     }
//     return ans;
// }

// function getColorSizes(colorData: any): Map<string, number> {
//     let sizes: Map<string, number> = new Map();
//     for (let line in colorData) {
//         let array = strToArray(line);
//         sizes = mergeMaps(sizes, countArray(array));
//     }
//     return sizes;
// }

// function countArray(array: string[]) {
//     let sizes: Map<string, number> = new Map();
//     array.forEach((key) => {
//       let size = sizes.get(key);
//       sizes.set(key, size ? size + 1 : 1);
//     });
//     return sizes;
//   }

// function mergeMaps(map1: Map<string, number>, map2: Map<string, number>): Map<string, number> {
//     map1.forEach((value, key) => {
//         let size = map2.get(key);
//         map1.set(key, size ? size + 1 : 1);
//     });
//     return map1;
//   }
  

// function strToArray(str: string): string[] {
//     str = str.replace(/'/g, '"');
//     return JSON.parse(str);
// }