import React from "react";
import Switch from "react-switch";
  
export const MaterialDesignSwitch: React.FC<{setToggleLabels: any}> = ({setToggleLabels}) => {
    const [checked, setChecked] = React.useState<boolean>(false);
    return (
        <div className="example">
          <h2>Toggle Labels</h2>
          <label htmlFor="material-switch">
            <Switch
              checked={checked}
              onChange={
                  (e) => {
                      setChecked(e);
                      setToggleLabels(e);
                    }
                }
              onColor="#86d3ff"
              onHandleColor="#2693e6"
              handleDiameter={30}
              uncheckedIcon={false}
              checkedIcon={false}
              boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
              activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
              height={15}
              width={48}
              className="react-switch"
              id="material-switch"
            />
          </label>
        </div>
      )
    }