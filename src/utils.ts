export function ArrayCommonElement(arr1: string[], arr2: string[]): boolean {
    return arr1.some(item => arr2.includes(item));
}