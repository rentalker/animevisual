export const axis = [
    { value: 'score', label: 'Score' },
    { value: 'rank', label: 'Rank' },
    { value: 'popularity', label: 'Popularity' },
    { value: 'favorites', label: 'Favorites'}
]

export const color = [
    // { value: 'type', label: 'Type' },
    // { value: 'source', label: 'Source' },
    { value: 'genre', label: 'Genre' },
    { value: 'producers', label: 'Producers'},
    { value: 'studio', label: 'Studio'},
    // { value: 'rating', label: 'Rating'},
]

export const label = [
    { value: 'title_english', label: 'Title English' },
    { value: 'title_japanese', label: 'Title Japanese' },
]

export const head = [
    { value: '1000', label: 'All' },
    { value: '10', label: '10' },
    { value: '25', label: '25' },
    { value: '50', label: '50' },
    { value: '100', label: '100' },
]